/*
' Copyright (c) 2017 Web Matrix
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System.Collections.Generic;
//using System.Xml;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Search;

namespace MPT.MPT_ShareModule.Components
{

    /// -----------------------------------------------------------------------------
    /// <summary>
    /// The Controller class for MPT_ShareModule
    /// 
    /// The FeatureController class is defined as the BusinessController in the manifest file (.dnn)
    /// DotNetNuke will poll this class to find out which Interfaces the class implements. 
    /// 
    /// The IPortable interface is used to import/export content from a DNN module
    /// 
    /// The ISearchable interface is used by DNN to index the content of a module
    /// 
    /// The IUpgradeable interface allows module developers to execute code during the upgrade 
    /// process for a module.
    /// 
    /// Below you will find stubbed out implementations of each, uncomment and populate with your own data
    /// </summary>
    /// -----------------------------------------------------------------------------

    //uncomment the interfaces to add the support.
    public class FeatureController //: IPortable, ISearchable, IUpgradeable
    {


        #region Optional Interfaces

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// ExportModule implements the IPortable ExportModule Interface
        /// </summary>
        /// <param name="ModuleID">The Id of the module to be exported</param>
        /// -----------------------------------------------------------------------------
        //public string ExportModule(int ModuleID)
        //{
        //string strXML = "";

        //List<MPT_ShareModuleInfo> colMPT_ShareModules = GetMPT_ShareModules(ModuleID);
        //if (colMPT_ShareModules.Count != 0)
        //{
        //    strXML += "<MPT_ShareModules>";

        //    foreach (MPT_ShareModuleInfo objMPT_ShareModule in colMPT_ShareModules)
        //    {
        //        strXML += "<MPT_ShareModule>";
        //        strXML += "<content>" + DotNetNuke.Common.Utilities.XmlUtils.XMLEncode(objMPT_ShareModule.Content) + "</content>";
        //        strXML += "</MPT_ShareModule>";
        //    }
        //    strXML += "</MPT_ShareModules>";
        //}

        //return strXML;

        //	throw new System.NotImplementedException("The method or operation is not implemented.");
        //}

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// ImportModule implements the IPortable ImportModule Interface
        /// </summary>
        /// <param name="ModuleID">The Id of the module to be imported</param>
        /// <param name="Content">The content to be imported</param>
        /// <param name="Version">The version of the module to be imported</param>
        /// <param name="UserId">The Id of the user performing the import</param>
        /// -----------------------------------------------------------------------------
        //public void ImportModule(int ModuleID, string Content, string Version, int UserID)
        //{
        //XmlNode xmlMPT_ShareModules = DotNetNuke.Common.Globals.GetContent(Content, "MPT_ShareModules");
        //foreach (XmlNode xmlMPT_ShareModule in xmlMPT_ShareModules.SelectNodes("MPT_ShareModule"))
        //{
        //    MPT_ShareModuleInfo objMPT_ShareModule = new MPT_ShareModuleInfo();
        //    objMPT_ShareModule.ModuleId = ModuleID;
        //    objMPT_ShareModule.Content = xmlMPT_ShareModule.SelectSingleNode("content").InnerText;
        //    objMPT_ShareModule.CreatedByUser = UserID;
        //    AddMPT_ShareModule(objMPT_ShareModule);
        //}

        //	throw new System.NotImplementedException("The method or operation is not implemented.");
        //}

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// GetSearchItems implements the ISearchable Interface
        /// </summary>
        /// <param name="ModInfo">The ModuleInfo for the module to be Indexed</param>
        /// -----------------------------------------------------------------------------
        //public DotNetNuke.Services.Search.SearchItemInfoCollection GetSearchItems(DotNetNuke.Entities.Modules.ModuleInfo ModInfo)
        //{
        //SearchItemInfoCollection SearchItemCollection = new SearchItemInfoCollection();

        //List<MPT_ShareModuleInfo> colMPT_ShareModules = GetMPT_ShareModules(ModInfo.ModuleID);

        //foreach (MPT_ShareModuleInfo objMPT_ShareModule in colMPT_ShareModules)
        //{
        //    SearchItemInfo SearchItem = new SearchItemInfo(ModInfo.ModuleTitle, objMPT_ShareModule.Content, objMPT_ShareModule.CreatedByUser, objMPT_ShareModule.CreatedDate, ModInfo.ModuleID, objMPT_ShareModule.ItemId.ToString(), objMPT_ShareModule.Content, "ItemId=" + objMPT_ShareModule.ItemId.ToString());
        //    SearchItemCollection.Add(SearchItem);
        //}

        //return SearchItemCollection;

        //	throw new System.NotImplementedException("The method or operation is not implemented.");
        //}

        /// -----------------------------------------------------------------------------
        /// <summary>
        /// UpgradeModule implements the IUpgradeable Interface
        /// </summary>
        /// <param name="Version">The current version of the module</param>
        /// -----------------------------------------------------------------------------
        //public string UpgradeModule(string Version)
        //{
        //	throw new System.NotImplementedException("The method or operation is not implemented.");
        //}

        #endregion

    }

}
